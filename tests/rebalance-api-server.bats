#!/usr/bin/env bats

teardown () {
  NODE2=`kubectl get no | head -n 3 | tail -n 1 | awk '{print $1}'`
  kubectl uncordon ${NODE2}
  kubectl -n cloudflare-loadbalancer patch cronjobs cleanup-apiserver -p '{"spec" : {"suspend" : false }}'
  kubectl -n cloudflare-loadbalancer delete --ignore-not-found=true job/cleanup-apiserver-now
  sleep 5

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "pod is running => ip is listed" {
  # ip exists
  NODE2=`kubectl get no | head -n 3 | tail -n 1 | awk '{print $1}'`
  POD=`kubectl -n cloudflare-loadbalancer get po -l app=loadbalancer-apiserver -o wide | grep ${NODE2} | awk '{print $1}'`
  run kubectl -n cloudflare-loadbalancer wait --for=condition=Ready pod/${POD}
  NODE2_IP=`kubectl -n cloudflare-loadbalancer exec ${POD} cat /informations/NODE_IP`
  [ $status -eq 0 ]
  sleep 120 # wait until dns cache time passed
  ECHOSERVER=`kubectl -n echoserver get ing echoserver | tail -n -1 | awk '{print $2}'`
  run sh -c "dig ${ECHOSERVER/echoserver/k8s} | grep ${NODE2_IP}"
  [ $status -eq 0 ]
}

@test "delete pod => ip is missing" {
  # ip exists
  NODE2=`kubectl get no | head -n 3 | tail -n 1 | awk '{print $1}'`
  POD=`kubectl -n cloudflare-loadbalancer get po -l app=loadbalancer-apiserver -o wide | grep ${NODE2} | awk '{print $1}'`
  run kubectl -n cloudflare-loadbalancer wait --for=condition=Ready pod/${POD}
  [ $status -eq 0 ]
  NODE2_IP=`kubectl -n cloudflare-loadbalancer exec ${POD} cat /informations/NODE_IP`
  sleep 120 # wait until dns cache time passed
  ECHOSERVER=`kubectl -n echoserver get ing echoserver | tail -n -1 | awk '{print $2}'`
  run sh -c "dig ${ECHOSERVER/echoserver/k8s} | grep ${NODE2_IP}"
  [ $status -eq 0 ]

  # ip gets deleted during pod deletion
  kubectl cordon ${NODE2}
  kubectl -n cloudflare-loadbalancer patch cronjobs cleanup-apiserver -p '{"spec" : {"suspend" : true }}'
  kubectl -n cloudflare-loadbalancer delete pod/${POD}
  run sh -c "dig ${ECHOSERVER/echoserver/k8s} | grep ${NODE2_IP}"
  [ $status -eq 1 ]
}
