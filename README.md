# Cloudflare DNS based load balancer

A Daemonset to use the pod lifecyle to add and remove node ips to a subdomain managed by cloudflare.

A Cronjob removes IPs of "hard" killed nodes/pods.

## Usage

run `make` for usage
