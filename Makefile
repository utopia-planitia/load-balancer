
include ../kubernetes/etc/help.mk
include ../kubernetes/etc/cli.mk

.PHONY: deploy
deploy: ##@setup deploy dns based load balancer to kubernetes
	$(CLI) kubectl apply -f namespace.yaml -f .

.PHONY: tests
tests: ##@development run tests
	$(CLI) bats tests/
